# frozen_string_literal: true

module ValidUserRequestHelper
  def sign_in_as_valid_user(user)
    visit new_user_session_path

    expect(page).to have_content 'Log in'
    within('form#new_user') do
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
    end
    click_button 'Log in'

    expect(page).to have_content 'Logout'
  end
end
