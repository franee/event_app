# frozen_string_literal: true

require 'rails_helper'

describe Event do
  it 'has a valid factory' do
    expect(build(:event).save).to be_truthy
  end

  it 'is invalid without an event type' do
    expect(build(:event, event_type: nil).save).to be_falsey
  end

  it 'is invalid without a speaker' do
    expect(build(:event, speaker: nil).save).to be_falsey
  end

  it 'is invalid without a start_at' do
    expect(build(:event, start_at: nil).save).to be_falsey
  end

  it 'is invalid without an end_at' do
    expect(build(:event, end_at: nil).save).to be_falsey
  end

  it 'is invalid without a name' do
    expect(build(:event, name: nil).save).to be_falsey
  end

  it 'is invalid without a location' do
    expect(build(:event, location: nil).save).to be_falsey
  end

  it 'is invalid without a description' do
    expect(build(:event, description: nil).save).to be_falsey
  end

  context 'event_type is a workshop' do
    it 'is invalid without max_participants' do
      expect(build(:event, event_type: 'workshop', max_participants: nil).save).to be_falsey
    end
  end

  context 'event_type is office_hours' do
    it 'is valid without max_participants' do
      expect(build(:event, event_type: 'office_hours', max_participants: nil).save).to be_truthy
    end

    context 'overlap: same name, speaker, location, time' do
      let!(:event) { create(:office_overlap) }

      it 'should not create event if start_at is the same' do
        expect(build(:office_overlap).save).to be_falsey
      end

      it 'should not create event if start_at is inside an existing time frame' do
        expect(build(:office_overlap, start_at: (event.start_at + 1.minute)).save).to be_falsey
      end

      it 'should not create event if end_at is inside an existing time frame' do
        expect(build(:office_overlap,
                     start_at: (event.start_at - 1.minute),
                     end_at: (event.start_at + 1.minute)).save).to be_falsey
      end
    end
  end

  context 'custom validators' do
    it 'is invalid when start_at is in the past' do
      expect(build(:event, start_at: 1.day.ago).save).to be_falsey
    end

    it 'is invalid when end_at is earlier than start_at' do
      expect(build(:event, start_at: Time.current, end_at: 1.day.ago).save).to be_falsey
    end

    it 'is invalid when end_at is the same as start_at' do
      now = Time.current
      expect(build(:event, start_at: now, end_at: now).save).to be_falsey
    end

    context 'multi-day' do
      it 'is invalid when start_at and end_at are not on the same day' do
        now = Time.current
        tom = 1.day.from_now
        expect(build(:event, start_at: now, end_at: tom).save).to be_falsey
      end
    end
  end
end
