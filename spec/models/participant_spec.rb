# frozen_string_literal: true

require 'rails_helper'

describe Participant do
  it 'has a valid factory' do
    expect(build(:participant).save).to be_truthy
  end

  it 'is invalid without an email' do
    expect(build(:participant, email: nil).save).to be_falsey
  end

  it 'is invalid without an event' do
    expect(build(:participant, event_id: nil).save).to be_falsey
  end

  context 'office_hours' do
    let!(:event) { create(:event, event_type: :office_hours, max_participants: 1) }

    before do
      create(:participant, event: event)
    end

    it 'should fail when adding an additional participant' do
      expect(build(:participant, event: event).save).to be_falsey
    end
  end

  context 'workshop' do
    let!(:event) { create(:event, event_type: :workshop, max_participants: 2) }

    before do
      create(:participant, event: event)
    end

    context 'within max_participant bounds' do
      it 'should be able to add an additional participant' do
        expect(build(:participant, event: event).save).to be_truthy
      end
    end

    context 'above max_participant limit' do
      it 'should fail' do
        expect(build(:participant, event: event).save).to be_truthy
        expect(build(:participant, event: event).save).to be_falsey
      end
    end
  end
end
