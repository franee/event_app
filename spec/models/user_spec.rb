# frozen_string_literal: true

require 'rails_helper'

describe User do
  it 'has a valid factory' do
    expect(build(:user).save).to be_truthy
    expect(build(:admin).save).to be_truthy
  end

  it 'has a default user role' do
    expect(create(:user).role).to eq('user')
  end

  context 'when admin role is set' do
    it 'should set the admin role' do
      expect(create(:admin).role).to eq('admin')
    end
  end
end
