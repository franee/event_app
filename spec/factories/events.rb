# frozen_string_literal: true

START_AT = 10.minutes.from_now.freeze
END_AT   = 40.minutes.from_now.freeze

FactoryBot.define do
  event_types = Event.event_types.keys
  event_type = event_types.sample
  max_participants = 1

  max_participants = Faker::Number.between(1, 100) if event_type == 'workshop'

  start_at = Faker::Time.forward(60, :day).beginning_of_hour
  end_at   = start_at + 1.hour

  factory :event do
    event_type       { event_type }
    speaker          { Faker::StarWars.character }
    start_at         { start_at }
    end_at           { end_at }
    name             { Faker::Commerce.department }
    location         { Faker::Address.city }
    description      { Faker::Lorem.sentence }
    max_participants { max_participants }
  end

  factory :office_overlap, class: Event do
    event_type       { 'office_hours' }
    speaker          { 'Speaker' }
    start_at         { START_AT }
    end_at           { END_AT }
    name             { 'Name' }
    location         { 'Location' }
    description      { Faker::Lorem.sentence }
    max_participants { 1 }
  end
end
