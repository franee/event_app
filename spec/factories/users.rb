# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email                   { Faker::Internet.email }
    password                'foobar'
    password_confirmation   'foobar'
    role                    'user'
  end

  factory :admin, class: User do
    email                   { Faker::Internet.email }
    password                'foobar'
    password_confirmation   'foobar'
    role                    'admin'
  end
end
