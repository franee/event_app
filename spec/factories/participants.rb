# frozen_string_literal: true

FactoryBot.define do
  factory :participant do
    email    { Faker::Internet.email }
    event_id do
      e = create(:event)
      e.id
    end
  end
end
