# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'events page' do
  it 'should see events elements' do
    expect(page).to have_content 'Event type'
    expect(page).to have_content 'Speaker'
  end
end

feature 'Can see events' do
  context 'regular user' do
    let!(:user) { create(:user) }

    before do
      sign_in_as_valid_user(user)
    end

    include_examples 'events page'
  end

  context 'admin user' do
    let!(:user) { create(:admin) }

    before do
      sign_in_as_valid_user(user)
    end

    include_examples 'events page'
  end

  context 'visitor' do
    before do
      visit root_path
    end

    include_examples 'events page'
  end
end
