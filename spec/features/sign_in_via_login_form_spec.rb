# frozen_string_literal: true

require 'rails_helper'

feature 'Signing-in via login form' do
  context 'regular user' do
    let!(:user) { create(:user) }

    it 'allows user to log in' do
      sign_in_as_valid_user(user)
      expect(page).not_to have_content 'Send and Invite'
    end
  end

  context 'admin user' do
    let!(:user) { create(:admin) }

    it 'allows user to log in' do
      sign_in_as_valid_user(user)
      expect(page).to have_content 'Send an Invite'
    end
  end
end
