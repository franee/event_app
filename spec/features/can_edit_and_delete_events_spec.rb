# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'edit & delete event' do
  it 'should not be able to edit event' do
    expect(page).not_to have_link 'Edit'
  end

  it 'should not be able to delete event' do
    expect(page).not_to have_link 'Destroy'
  end
end

feature 'Can edit & delete events' do
  context 'regular user' do
    let!(:user) { create(:user) }

    before do
      create(:event)
      sign_in_as_valid_user(user)
    end

    include_examples 'edit & delete event'
  end

  context 'admin user' do
    let!(:user) { create(:admin) }

    before do
      create(:event)
      sign_in_as_valid_user(user)
    end

    it 'should be able to edit event' do
      expect(page).to have_link 'Edit'
    end

    it 'should be able to delete event' do
      expect(page).to have_link 'Destroy'
    end
  end

  context 'visitor' do
    before do
      create(:event)
      visit root_path
    end

    include_examples 'edit & delete event'
  end
end
