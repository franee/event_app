# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'create event' do
  it 'should be able to view new event' do
    expect(page).to have_link 'New Event'
  end
end

feature 'Can create events' do
  context 'regular user' do
    let!(:user) { create(:user) }

    before do
      sign_in_as_valid_user(user)
    end

    include_examples 'create event'
  end

  context 'admin user' do
    let!(:user) { create(:admin) }

    before do
      sign_in_as_valid_user(user)
    end

    include_examples 'create event'
  end

  context 'visitor' do
    before do
      visit root_path
    end

    it 'should not be able to view new event' do
      expect(page).not_to have_link 'New Event'
    end
  end
end
