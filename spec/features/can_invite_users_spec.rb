# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'invite users' do
  it 'should not be able to send an invite' do
    expect(page).not_to have_link 'Send an Invite'
  end
end

feature 'Can invite users' do
  context 'regular user' do
    let!(:user) { create(:user) }

    before do
      sign_in_as_valid_user(user)
    end

    include_examples 'invite users'
  end

  context 'admin user' do
    let!(:user) { create(:admin) }

    before do
      sign_in_as_valid_user(user)
    end

    it 'should be invite users' do
      expect(page).to have_link 'Send an Invite'
    end
  end

  context 'visitor' do
    before do
      visit root_path
    end

    include_examples 'invite users'
  end
end
