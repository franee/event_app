class CreateParticipants < ActiveRecord::Migration[5.2]
  def change
    create_table :participants do |t|
      t.string :email
      t.integer :event_id

      t.timestamps
    end

    add_index :participants, [:event_id, :email]
  end
end
