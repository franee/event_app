# frozen_string_literal: true

class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.integer :event_type
      t.string :speaker
      t.datetime :start_at
      t.datetime :end_at
      t.string :name
      t.string :location
      t.text :description
      t.integer :max_participants

      t.timestamps
    end

    add_index :events, :event_type
    add_index :events, :speaker
    add_index :events, :start_at
    add_index :events, :location
  end
end
