# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

abort('Only run this on development & production envs!') if Rails.env.test?

require 'faker'

# admin user
user = User.new(email: 'admin@example.com')
user.password = 'foobar'
user.password_confirmation = 'foobar'
user.role = :admin
user.save

# regular user
user = User.new(email: 'user@example.com')
user.password = 'foobar'
user.password_confirmation = 'foobar'
user.save

event_types = Event.event_types.keys

1.upto(100) do
  event_type = event_types.sample
  max_participants = 1

  max_participants = Faker::Number.between(1, 100) if event_type == 'workshop'

  start_at = Faker::Time.forward(60, :day).beginning_of_hour
  end_at   = start_at + 1.hour

  Event.create(
    event_type: event_type,
    speaker:    Faker::StarWars.character,
    start_at:   start_at,
    end_at:     end_at,
    name:       Faker::Commerce.department,
    location:   Faker::Address.city,
    description: Faker::Lorem.sentence,
    max_participants: max_participants
  )
end
