Time::DATE_FORMATS[:short_ordinal] = lambda { |time| time.strftime("#{time.day.ordinalize} %B %Y" ) }
Time::DATE_FORMATS[:event_duration] = lambda { |time| time.strftime("%Hh%M") }
