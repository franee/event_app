Rails.application.routes.draw do
  devise_for :users

  resources :events do
    resources :participants
  end

  mount Magic::Link::Engine, at: '/'

  root 'events#index'
end
