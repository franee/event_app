# README

## setup

Make sure nodejs is installed. uglifier gem needs it.

```bash
$ bundle install
$ rake db:reset
```
add  gmail credentials on config/application.yml to configure mailer
```bash
gmail_username: 'username@gmail.com'
gmail_password: 'password'
````

## run specs

```bash
$ rake spec
```

## start app

```bash
$ rails server
```

## credentials
admin user: admin@example.com / foobar
normal user: user@example.com / foobar
