# frozen_string_literal: true

class EventMailer < ApplicationMailer
  default from: 'notifier@coinhouse_events.com'
  layout 'mailer'

  def sample_email(user)
    @user = user
    mail(to: @user.email, subject: 'Sample Email')
  end
end
