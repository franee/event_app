# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'from@coinhousechallenge.com'
  layout 'mailer'

  def sample_email(user)
    @user = user
    mail(to: @user.email, subject: 'Sample Email')
  end
end
