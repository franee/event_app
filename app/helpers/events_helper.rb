# frozen_string_literal: true

module EventsHelper
  def start_date(date)
    date.to_formatted_s(:short_ordinal)
  end

  def duration(start_at, end_at)
    ['between', start_at.to_formatted_s(:event_duration),
     'and',     end_at.to_formatted_s(:event_duration)].join(' ')
  end

  def ratio(event)
    "#{event.participants.size} / #{event.max_participants}"
  end
end
