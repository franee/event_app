# frozen_string_literal: true

class EventsController < ApplicationController
  # rubocop:disable Style/SafeNavigation
  before_action only: %i[destroy] do
    redirect_to new_user_session_path unless current_user && current_user.admin?
  end
  # rubocop:enable Style/SafeNavigation

  before_action :authenticate_user!, only: %i[new update create]
  before_action :set_event, only: %i[show edit update destroy]
  before_action :set_event_types, only: %i[new edit create update]

  # GET /events
  # GET /events.json
  def index
    if params[:term]
      search_events
    else
      @events = Event.all.includes(:participants).page params[:page]
    end
  end

  # rubocop:disable Style/EmptyMethod
  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/1/edit
  def edit
  end
  # rubocop:enable Style/EmptyMethod

  # GET /events/new
  def new
    @event = Event.new
    @event_types = Event.event_types
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def search_params
    params.permit(:page, :term)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end

  def set_event_types
    @event_types = Event.event_types
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def event_params
    params.require(:event).permit(:event_type, :speaker, :start_at, :end_at, :name,
                                  :location, :description, :max_participants)
  end

  def search_events
    case params[:term]
    when /off/
      event_type = Event.event_types[:office_hours]
      @events = Event.where('event_type = ?', event_type).includes(:participants).all.page params[:page]
    when /work/
      event_type = Event.event_types[:workshop]
      @events = Event.where('event_type = ?', event_type).includes(:participants).all.page params[:page]
    when /\d/
      date = Date.parse(params[:term])
      @events = Event.where('DATE(start_at) = ?', date.to_s).includes(:participants).all.page params[:page]
    else
      term = "%#{params[:term]}%"
      @events = Event.where('speaker LIKE ? OR location LIKE ?', term, term)
                     .includes(:participants).all.page params[:page]
    end
  end
end
