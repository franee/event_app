# frozen_string_literal: true

class Participant < ApplicationRecord
  belongs_to :event

  validates :email, presence: true
  validate :event_max_participants

  private

  def event_max_participants
    return unless event && event.participants.count >= event.max_participants.to_i

    errors.add(:base, 'participant limit exceeded')
  end
end
