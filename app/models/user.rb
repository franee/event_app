# frozen_string_literal: true

class User < ApplicationRecord
  enum role: %i[user admin]
  after_initialize :set_default_role, if: :new_record?

  def set_default_role
    role || :user
  end

  devise :database_authenticatable, :invitable, :validatable
end
