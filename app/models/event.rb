# frozen_string_literal: true

class Event < ApplicationRecord
  has_many :participants, dependent: :destroy

  validates :event_type, presence: true
  validates :speaker, presence: true
  validates :start_at, presence: true
  validates :end_at, presence: true
  validates :name, presence: true
  validates :location, presence: true
  validates :description, presence: true
  validates :max_participants, presence: true, if: :workshop?

  enum event_type: %i[workshop office_hours]

  # custom validators

  validate :start_at_cannot_be_in_the_past
  validate :end_at_cannot_be_earlier_than_start_at
  validate :end_at_should_be_the_same_day_as_start_at
  validate :no_office_hour_time_overlap

  private

  # rubocop:disable Rails/FindBy
  def no_office_hour_time_overlap
    return if workshop?
    record = Event.where("name = ? AND speaker = ? AND location = ? AND ((start_at BETWEEN ? AND ?) \
                         OR (end_at BETWEEN ? AND ?))",
                         name, speaker, location, start_at, end_at, start_at, end_at).take

    errors.add(:start_at, 'timeslot already taken') if record
  end
  # rubocop:enable Rails/FindBy

  def workshop?
    event_type == 'workshop'
  end

  def start_at_cannot_be_in_the_past
    errors.add(:start_at, "can't be in the past") if start_at && start_at < Time.current
  end

  def end_at_cannot_be_earlier_than_start_at
    errors.add(:end_at, "can't be earlier than start_at") if end_at && start_at && end_at <= start_at
  end

  # no multi-day events
  def end_at_should_be_the_same_day_as_start_at
    if end_at && start_at && end_at > start_at &&
       end_at.to_date != start_at.to_date
      errors.add(:end_at, 'should be on the same day as start_at')
    end
  end
end
